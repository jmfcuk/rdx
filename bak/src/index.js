import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import { createStore, compose } from 'redux';
import { Provider, connect } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';

// Reducer
const items = (state = [], action) => {
    switch (action.type) {
        case 'CREATE_ITEM':
            return state.concat([action.data])
        case 'DELETE_ITEM':
            return state.filter((val, idx) => {
                return val !== action.data;
            });
        default:
            return state;
    }
};

// Actions
const createItem = (text) => {
    return {
        type: 'CREATE_ITEM',
        data: text
    }
}
const deleteItem = (text) => {
    return {
        type: 'DELETE_ITEM',
        data: text
    }
}

// Store
const store = createStore(
    items,
    undefined,
    compose(
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
)

// Component
class App extends Component {

    constructor() {
        super();
    }

    onSubmit = (e) => {

        e.preventDefault();
        const item = e.target.querySelector('input').value;
        this.props.createItem(item);
    }

    onDeleteItem = (e) => {
        const item = e.currentTarget.textContent;
        this.props.deleteItem(item);
    }

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    <input type="text" />
                    <button type="submit">button</button>
                </form>
                <br />
                {this.props.items.map((text, id) => (
                    <div key={id} onClick={this.onDeleteItem}>
                        {text}
                    </div>
                ))}
            </div>
        );
    }
}

// Container
const MyApp = connect(state => ({
    items: state
}), { createItem, deleteItem })(App);

ReactDOM.render(
    <Provider store={store}>
        <MyApp />
    </Provider>,
    document.getElementById('root'));

registerServiceWorker();
