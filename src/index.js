import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import { createStore, compose } from 'redux';
import { Provider, connect } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';

// Reducer
const itemsState = (state = { items: [], editingItem: '' }, action) => {
    switch (action.type) {
        case 'CREATE': {
            return { items: state.items.concat([action.data]), editingItem: '' };
        }
        case 'BEGIN_EDIT': {
            return { items: state.items, editingItem: action.data };
        }
        case 'SAVE_EDIT': {
            return state;
        }
        case 'DELETE': {
            const its = state.items.filter((val, idx) => {
                return val !== action.data;
            });
            return { items: its, editingItem: '' };
        }
        default:
            return state;
    }
};

// Actions
const createItem = (item) => {
    return {
        type: 'CREATE',
        data: item
    }
};
const editItem = (item) => {
    return {
        type: 'BEGIN_EDIT',
        data: item
    }
};
const saveItem = (oldItem, newItem) => {
    return {
        type: 'SAVE_EDIT',
        data: {
            oldVal: oldItem,
            newVal: newItem
        }
    }
};
const deleteItem = (item) => {
    return {
        type: 'DELETE',
        data: item
    }
};

// Store
const store = createStore(
    itemsState,
    undefined,
    compose(
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);

// Component
class App extends Component {

    state = {
        editedItem: this.props.editingItem
    }

    onSubmit = (e) => {

        e.preventDefault();
        const item = e.target.querySelector('input').value;
        this.props.createItem(item);
    }

    onEditItem = (e, item) => {
        this.props.editItem(item);
    }

    onChangeItem = (e) => {
        const newItem = e.target.value;
        this.setState({ editedItem: newItem });
    }

    onSaveItem = () => {
        this.props.saveItem(this.props.editingItem, this.state.editedItem);
        this.setState({ editedItem: '' });
    }

    onDeleteItem = (e, item) => {
        e.stopPropagation();
        this.props.deleteItem(item);
    }

    render() {

        const frm = (
            <form onSubmit={this.onSubmit}>
                <input type="text" />
                <button type="submit">Add</button>
            </form>
        );

        const hidden = {
            display: 'none'
        }

        let edItem = this.props.editingItem;

        return (
            <div>
                {frm}
                <br />
                {this.props.items.map((item, idx) => (
                    <div key={idx + 1}>
                        <div key={idx} style={item === edItem ? hidden : {}} onClick={e => this.onEditItem(e, item)}>
                            {item} <span key={idx} onClick={e => this.onDeleteItem(e, item)}>X</span>
                        </div>
                        <div key={idx + 1 * 10} style={item === edItem ? {} : hidden}>
                            <span>
                                <input
                                    defaultValue={item}
                                    onChange={e => this.onChangeItem(e)}
                                />
                                <button onClick={e => this.onSaveItem()}>Save</button>
                            </span>
                        </div>
                    </div>
                ))}
            </div>
        );
    }
};

// Container
const MyApp = connect(state => ({
    items: state.items,
    editingItem: state.editingItem
}), { createItem, editItem, saveItem, deleteItem })(App);

ReactDOM.render(
    <Provider store={store}>
        <MyApp />
    </Provider>,
    document.getElementById('root'));

registerServiceWorker();
